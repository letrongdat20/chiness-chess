import pygame
import sys
from pygame.locals import *

from display.screens.Setting import Setting
from display.screens.Game2Player import Game2Player
from display.screens.Game1Player import Game1Player

from manament.const import *
from manament.confic import Confic

# Class that display menu
class Menu:
    # Singleton
    _instance = None

    @staticmethod
    def getInstance():
        if Menu._instance == None:
            Menu()
        return Menu._instance

    def __init__(self):
        if Menu._instance != None:
            raise Exception("This is singleton class")
        Menu._instance = self

    # Display screen to window
    # @param WINDOW {surface} app window
    def render(self, WINDOW):
        if(WINDOW == None):
            return False
        fpsClock = pygame.time.Clock()

        # Create button (X axis, Y axis, width, height)
        button1 = pygame.Rect(360, 150, 200, 60)
        button2 = pygame.Rect(360, 250, 200, 60)
        button3 = pygame.Rect(360, 350, 200, 60)
        button4 = pygame.Rect(360, 450, 200, 60)

        # Create title of button
        font = pygame.font.Font("freesansbold.ttf", 32)
        text1 = font.render("2 Player", True, WHITE)    # title
        text2 = font.render("1 Player", True, WHITE)
        text3 = font.render("Setting", True, WHITE)
        text4 = font.render("Exit", True, WHITE)

        # Create frame of title and set position for frame
        textRect1 = text1.get_rect()
        textRect1.center = (460, 180)
        textRect2 = text2.get_rect()
        textRect2.center = (460, 280)
        textRect3 = text3.get_rect()
        textRect3.center = (460, 380)
        textRect4 = text4.get_rect()
        textRect4.center = (460, 480)

        while True:
            # Game loop of pygame
            loop = True
            redirect = None

            # Background initialization
            Confic.loadConfic()
            background = pygame.image.load('display\\images\\bg\\' + Confic._backgroundType +'.jpg')
            background = pygame.transform.scale(
            background, (WINDOWN_WIDTH, WINDOWN_HEIGHT))
            while loop:
                # Handle event
                x_mouse, y_mouse = pygame.mouse.get_pos()
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        pygame.quit()
                        sys.exit()
                    if event.type == pygame.MOUSEBUTTONDOWN:
                        if(x_mouse > 50 and y_mouse > 50 and x_mouse < (CONTAINER_WIDTH + 50)
                           and y_mouse < (CONTAINER_HEIGHT + 50)):
                            if event.button == 1:
                                if button1.collidepoint((x_mouse, y_mouse)):
                                    redirect = "2Player"
                                    loop = False
                                elif button2.collidepoint((x_mouse, y_mouse)):
                                    redirect = "1Player"
                                    loop = False
                                elif button3.collidepoint((x_mouse, y_mouse)):
                                    redirect = "Setting"
                                    loop = False
                                elif button4.collidepoint((x_mouse, y_mouse)):
                                    redirect = "Exit"
                                    loop = False

                # Fill background
                WINDOW.fill(WHITE)
                WINDOW.blit(background, (0, 0))
                pygame.draw.rect(WINDOW, PRIMARY, button1)
                WINDOW.blit(text1, textRect1)
                pygame.draw.rect(WINDOW, SUCCESS, button2)
                WINDOW.blit(text2, textRect2)
                pygame.draw.rect(WINDOW, DANGER, button3)
                WINDOW.blit(text3, textRect3)
                pygame.draw.rect(WINDOW, INFOR, button4)
                WINDOW.blit(text4, textRect4)

                pygame.display.update()
                fpsClock.tick(FPS)

            # redirect
            if redirect == "2Player":
                game = Game2Player.getInstance()
                game.render(WINDOW)
            elif redirect == "Setting":
                setting = Setting.getInstance()
                setting.render(WINDOW)
            elif redirect == "Exit":
                pygame.quit()
                sys.exit()
            elif redirect == "1Player":
                game = Game1Player.getInstance()
                game.render(WINDOW)
            else:
                pygame.quit()
                sys.exit()

    # run GUI and initialize window
    def run(self):
        # Setting game window
        pygame.init()
        WINDOW = pygame.display.set_mode((WINDOWN_WIDTH, WINDOWN_HEIGHT))
        pygame.display.set_caption('Chinese Chess')
        self.render(WINDOW)
