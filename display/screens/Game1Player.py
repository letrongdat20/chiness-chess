import pygame
import sys
from pygame.locals import *
from manament import confic
from display.components.Board import Board
from manament.const import *

# Class that display board (main game)
class Game1Player:
    # Singleton
    _instance = None

    @staticmethod
    def getInstance():
        if Game1Player._instance == None:
            Game1Player()
        return Game1Player._instance

    def __init__(self):
        if Game1Player._instance != None:
            raise Exception("This is singleton class")
        self.container = pygame.Surface(
            (CONTAINER_WIDTH, CONTAINER_HEIGHT), SRCALPHA)
        # Class which display board and piece
        self.board = Board(confic.BoardConfic, confic.PieceConfic)
        # Class that display information of player
        self.info = pygame.Surface(
            (CONTAINER_WIDTH - BOARD_WIDTH, BOARD_HEIGHT))
        Game1Player._instance = self

    def getSurface(self):
        board = self.board.getBoard()
        container = self.container.copy()
        container.blit(board, (0, 0))
        container.blit(self.info, (BOARD_WIDTH, 0))
        return container

    def onClick(self, x_mouse, y_mouse):
        if (x_mouse < BOARD_WIDTH) and (y_mouse < BOARD_HEIGHT):
            if (x_mouse > 0) and (y_mouse > 0):
                self.board.chosePiece(x_mouse, y_mouse)    # If click on piece

    def getType(self):
        return "Game 1 Player"

    def render(self, WINDOW):
        if(WINDOW == None):
            return False
        fpsClock = pygame.time.Clock()
        # Background initialization
        background = pygame.image.load(r'display\images\bg\bg.jpg')
        background = pygame.transform.scale(
            background, (WINDOWN_WIDTH, WINDOWN_HEIGHT))

        # Exit button
        font = pygame.font.Font("freesansbold.ttf", 28)
        text = font.render("Back", True, (255, 0, 0))
        textRect = text.get_rect()
        textRect.center = (85, 30)

        # Loop game
        loop = True
        while loop:
            # Handle event
            x_mouse, y_mouse = pygame.mouse.get_pos()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if (x_mouse > 50) and (y_mouse > 50) and (x_mouse < CONTAINER_WIDTH + 50)\
                            and (y_mouse < CONTAINER_HEIGHT + 50):
                        self.onClick(x_mouse - 50, y_mouse - 50)
                    if textRect.collidepoint((x_mouse, y_mouse)):
                        loop = False    # Exit

            # Fill background
            WINDOW.fill(WHITE)
            WINDOW.blit(background, (0, 0))

            # Render current window
            container = self.getSurface()
            WINDOW.blit(container, (50, 50))
            WINDOW.blit(text, textRect)

            pygame.display.update()
            fpsClock.tick(FPS)
        
        # Break while = back to menu

    # Run GUI and initialize window
    def run(self):
        # Setting game window
        pygame.init()
        WINDOW = pygame.display.set_mode((WINDOWN_WIDTH, WINDOWN_HEIGHT))
        pygame.display.set_caption('Chinese Chess')
        self.render(WINDOW)
