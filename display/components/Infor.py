import pygame
from manament.const import *
from pygame.locals import *


class Infor:
    def __init__(self):
        self.container = pygame.Surface(
            (CONTAINER_WIDTH - BOARD_WIDTH, BOARD_HEIGHT), SRCALPHA)   # create surface
        self.setPieces()

    def setPieces(self, style="WOOD", player1_piece="B", player2_piece="R"):
        self.player_1 = pygame.transform.scale(pygame.image.load(
            "display\\images\\piece\\" + style + "\\" + player1_piece + "K.GIF"), (60, 60))
        self.player_2 = pygame.transform.scale(pygame.image.load(
            "display\\images\\piece\\" + style + "\\" + player2_piece + "K.GIF"), (60, 60))
        self.build()

    def build(self):
        font = pygame.font.Font("freesansbold.ttf", 48)  # write title
        text = font.render("Board", True, PRIMARY)
        rect = text.get_rect()
        rect.center = ((CONTAINER_WIDTH - BOARD_WIDTH)//2, 25)
        img = pygame.image.load("display\\images\\scoreboard\\scoreboard_eclipse.png")
        scoreboard = pygame.transform.scale(img, (320, 180))
        self.container.blit(text, rect)
        self.container.blit(scoreboard, ((CONTAINER_WIDTH - BOARD_WIDTH)//2 - 160, 50))
        self.container.blit(scoreboard, ((CONTAINER_WIDTH - BOARD_WIDTH)//2 - 160, CONTAINER_HEIGHT//2))
        self.container.blit(self.player_1, ((CONTAINER_WIDTH - BOARD_WIDTH)//2 - 80, 120))
        self.container.blit(self.player_2, ((CONTAINER_WIDTH - BOARD_WIDTH)//2 - 80, CONTAINER_HEIGHT//2 + 70))

    def getSurface(self):
        container = self.container.copy()
        font = pygame.font.Font("freesansbold.ttf", 32)  # front
        # score of player 1
        score_1 = font.render("player 1", True, WHITE)
        rect = score_1.get_rect()
        rect.center = ((CONTAINER_WIDTH - BOARD_WIDTH)//2 + 30, 150)
        # score of player 2
        score_2 = font.render("player 2", True, WHITE)
        rect2 = score_2.get_rect()
        rect2.center = ((CONTAINER_WIDTH - BOARD_WIDTH)//2 + 30, CONTAINER_HEIGHT//2 + 100)

        self.container.blit(score_1, rect)
        self.container.blit(score_2, rect2)
        return container

    def setData(self):
        pass

    # return current data
    def getData(self):
        pass

    # return pygame.Rect class of Undo button
    def getUndoRect(self):
        pass

    # return pygame.Rect class of Redo button
    def getRedoRect(self):
        pass

    # return pygame.Rect class of Reset button
    def getResetRect(self):
        pass
