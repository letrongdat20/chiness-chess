import pygame
from manament.match import Match
from manament.const import *

# load and resize piece from image, add to dictionary
def pieceConvert(type, size):
	dictionary = {}
	pieces = PIECES
	for piece in pieces:
		dictionary[piece] = pygame.transform.scale(pygame.image.load(
			'display\\images\piece\\' + type + '\\' + piece + '.GIF').convert(), size)
	return dictionary

'''
	Class that will display board and piece
	@property:
		size {int(pixel)}: size of board, get from const file
		surface {class}: class of pygame that will render board to screen
		piece {dict}: dictionary of pieces which are surface of piece
		match {class}: own class that manage match by symbol 
		state {array}: a array 10*9 that will is matrix of piece
		turn {string}: symbol of player
		chosed {tuple}: position of piece that clicked, default is (-1, -1)
	@param:
		board {string} type of board
		piece {string} type of piece
'''
class Board:
	def __init__(self, board, piece):
		self.size = (BOARD_WIDTH, BOARD_HEIGHT)
		self.surface = pygame.image.load(
			'display\\images\\board\\' + board + '.png').convert()
		self.chosed = (-1, -1)  # there are not piece that chosed
		self.piece = pieceConvert(piece, PIECE_SIZE)
		self.match = Match()
		self.state = self.match.getState()
		self.turn = self.match.getTurn()

	#return surface that will render board and piece
	def getBoard(self):
		surface = pygame.transform.scale(self.surface, self.size)

		#draw piece into board
		for i in range(len(self.state)):
			for j in range(len(self.state[i])):
				surface.blit(self.piece[self.state[i][j]],
							 (j*65 + 10, i*60 - 2 - (i // 5)*4))
		return surface

	def isWin(self):
		return self.match.isWin

	def winner(self):
		return self.match.winner

	def updateState(self):
		self.state = self.match.getState()
		self.turn = self.match.getTurn()

	# set type and size of board
	def setBoard(self, board):
		temp = pygame.image.load(
			'display\\images\\board\\' + board + '.GIF')
		self.surface = pygame.transform.scale(temp, self.size)
		self.reset()

	def reset(self):
		self.match.reset()
		self.chosed = (-1, -1)
		self.updateState()

	# process when a piece clicked
	def chosePiece(self, x_mouse, y_mouse):
		if((x_mouse < 10) or (y_mouse < 0)):
			return False
		if((x_mouse > BOARD_WIDTH - 10) or (y_mouse > BOARD_HEIGHT - 8)):
			return False
		#find position of piece
		j = (x_mouse - 10) // 65
		i = (y_mouse + 2) // 60
		i_old, j_old = self.chosed

		# if click on a piece
		if(self.state[i][j] != 'OO'):
			if(self.state[i][j][0 : 1] == self.turn):    # click on type of piece that have turn
				if(i_old != -1 and j_old != -1):    # if there is a piece that chosed, it must be remove
					self.state[i_old][j_old] = self.state[i_old][j_old][0: 2]
				self.state[i][j] = self.state[i][j] + 'S'
				self.chosed = (i, j)

			# if click on type of piece that does not have turn
			else:
				if(i_old != -1 and j_old != -1):
					self.match.move(src=(i_old, j_old), des=(i, j))
					self.updateState()

		# if click on position that does not have piece
		elif(self.state[i][j] == 'OO'):
			if(i_old != -1 and j_old != -1):    # if there has not piece that chosed 
				self.state[i_old][j_old] = self.state[i_old][j_old][0: 2]
				self.match.move(src=(i_old, j_old), des=(i, j))
				self.updateState()
	
	# go back one step
	def undo(self):
		self.match.undo()
		self.updateState()

	# go to next step
	def redo(self):
		self.match.redo()
		self.updateState()