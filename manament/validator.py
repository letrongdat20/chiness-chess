import copy
from manament.const import *

class Validator:
	__x_old = -1  # old row of piece
	__x_new = -1  # new row of piece
	__y_old = -1  # old column of piece
	__y_new = -1  # new column of piece
	__turn = "O"  # symbol of player
	__state = None  # state of game
	__piece = "O"  # type of piece

	#    Check if row is invalid
	@staticmethod
	def xNotInvalid(row):
		if((row < 0) or (row > ROW)):
			return True
		return False

	#   Check if column is invalid
	@staticmethod
	def yNotInvalid(col):
		if((col < 0) or (col > COL)):
			return True
		return False

		''' Check move of piece include:
			Check if move is not in board
			Check if move to position have ally piece
			Check if move does not change position
			Check if move is invalid
		src is tuple of old position, des is tuple of new position
	'''
	@staticmethod
	def check(state, src, des):
		Validator.__state = copy.deepcopy(state)
		Validator.__x_old, Validator.__y_old = src
		Validator.__x_new, Validator.__y_new = des
		Validator.__turn = state[Validator.__x_old][Validator.__y_old][0:1]
		if(Validator.xNotInvalid(Validator.__x_old) or Validator.xNotInvalid(Validator.__x_new)):
			return False
		if(Validator.yNotInvalid(Validator.__y_old) or Validator.yNotInvalid(Validator.__y_new)):
			return False
		if(state[Validator.__x_new][Validator.__y_new][0:1] == state[Validator.__x_old][Validator.__y_old][0:1]):
			return False  # move to position that have ally
		Validator.__piece = state[Validator.__x_old][Validator.__y_old][1:2]
		#	src and des is same
		if(Validator.__x_new == Validator.__x_old):
			if(Validator.__y_new == Validator.__y_old):
				return False

		if(Validator.__piece == "K"):
			return Validator.checkKing()
		if(Validator.__piece == "A"):
			return Validator.checkAdvisor()
		if(Validator.__piece == "B"):
			return Validator.checkElephant()
		if(Validator.__piece == "N"):
			return Validator.checkHorse()
		if(Validator.__piece == "R"):
			return Validator.checkChariot()
		if(Validator.__piece == "C"):
			return Validator.checkCannon()
		if(Validator.__piece == "P"):
			return Validator.checkSoldier()
		return False

	#   Check if 2 King is opposite
	@staticmethod
	def kingIsOpposite():
		copyState = copy.deepcopy(Validator.__state)  # copy and update state
		copyState[Validator.__x_new][Validator.__y_new] = copyState[Validator.__x_old][Validator.__y_old]
		copyState[Validator.__x_old][Validator.__y_old] = "OO"

		if(Validator.__y_old == Validator.__y_new):  # if move in a col king is not opposite
			return False
		numKing = 0  # number of king in col
		for i in range(ROW):
			if(copyState[i][Validator.__y_old][1:2] == "K"):
				numKing += 1
				continue
			if(numKing == 2):
				return True
			# if there are a piece between king
			if((numKing == 1) and (copyState[i][Validator.__y_old] != "OO")):
				return False
		if(numKing == 2):
			return True

	#   Check if move of king if invalid
	@staticmethod
	def checkKing():
		# Check if King in palace
		if(not (Validator.__x_new <= 2 or Validator.__x_new >= 7)):
			return False
		if(Validator.__y_new > 5 or Validator.__y_new < 3):
			return False

		# Check if move in one step
		if(Validator.__x_old == Validator.__x_new):
			if((Validator.__y_old - Validator.__y_new == 1) or (Validator.__y_new - Validator.__y_old == 1)):
				return True
		elif(Validator.__y_old == Validator.__y_new):
			if((Validator.__x_old - Validator.__x_new == 1) or (Validator.__x_new - Validator.__x_old == 1)):
				return True
		return False

	#   Check move of Advisor if invalid
	@staticmethod
	def checkAdvisor():
		# Check if Advisor in palace
		if(not (Validator.__x_new <= 2 or Validator.__x_new >= 7)):
			return False
		if(Validator.__y_new > 5 or Validator.__y_new < 3):
			return False

		#	Check if move cross
		if((Validator.__x_old - Validator.__x_new == 1) or (Validator.__x_new - Validator.__x_old == 1)):
			if((Validator.__y_old - Validator.__y_new == 1) or (Validator.__y_new - Validator.__y_old == 1)):
				return True
		return False

	#   Check move of Elephant if invalid
	@staticmethod
	def checkElephant():
		#	Check if elephant over river
		if((Validator.__x_new > 4) and (Validator.__x_old < 4)):
			return False
		if((Validator.__x_new < 4) and (Validator.__x_old > 4)):
			return False

		#	Check if move cross
		if((Validator.__x_old - Validator.__x_new == 2) or (Validator.__x_new - Validator.__x_old == 2)):
			if((Validator.__y_old - Validator.__y_new == 2) or (Validator.__y_new - Validator.__y_old == 2)):
				#	Check if elephant is blocked
				m = (Validator.__x_old + Validator.__x_new)//2
				n = (Validator.__y_old + Validator.__y_new)//2
				if(Validator.__state[m][n] != "OO"):
					return False
				return True
		return False

	#   Check move of Horse if invalid
	@staticmethod
	def checkHorse():
		if((Validator.__x_old - Validator.__x_new == 2) or (Validator.__x_new - Validator.__x_old == 2)):
			if((Validator.__y_old - Validator.__y_new == 1) or (Validator.__y_new - Validator.__y_old == 1)):
				#	Check if Horse is blocked
				if(Validator.__state[(Validator.__x_new + Validator.__x_old)//2][Validator.__y_old] != "OO"):
					return False
				return True
		if((Validator.__x_old - Validator.__x_new == 1) or (Validator.__x_new - Validator.__x_old == 1)):
			if((Validator.__y_old - Validator.__y_new == 2) or (Validator.__y_new - Validator.__y_old == 2)):
				#	Check if Horse is blocked
				if(Validator.__state[Validator.__x_old][(Validator.__y_new + Validator.__y_old)//2] != "OO"):
					return False
				return True
		return False

	#   Check move of Chariot if invalid
	@staticmethod
	def checkChariot():
		#	Chariot goes in a straight line
		if(Validator.__x_old == Validator.__x_new):
			#	Check if Chariot is blocked
			if Validator.__y_new > Validator.__y_old:  # if new > old, travel from new to old
				temp = 1  # temp is direction to travel
			else:  # if new < old, travel from new to old
				temp = -1
			for j in range(Validator.__y_old + temp, Validator.__y_new, temp):
				if(Validator.__state[Validator.__x_old][j] != "OO"):
					return False
			return True
		if(Validator.__y_old == Validator.__y_new):
			#	Check if Chariot is blocked
			if Validator.__x_new > Validator.__x_old:  # if new > old, travel from old to new
				temp = 1  # temp is direction to travel
			else:  # if new < old, travel from new to old
				temp = -1
			for i in range(Validator.__x_old + temp, Validator.__x_new, temp):
				if(Validator.__state[i][Validator.__y_old] != "OO"):
					return False
			return True
		return False

	#   Check move of Cannon if invalid
	@staticmethod
	def checkCannon():
		numPiece = 0  # number of piece between old position and new position
		if(Validator.__y_old == Validator.__y_new):
			if Validator.__x_new > Validator.__x_old:  # if new > old, travel from old to new
				temp = 1  # temp is direction to travel
			else:  # if new < old, travel from new to old
				temp = -1
			# Count num of piece between old position and new position
			for i in range(Validator.__x_old + temp, Validator.__x_new, temp):
				if(Validator.__state[i][Validator.__y_old] != "OO"):
					numPiece += 1
			# there is not piece between old and new position
			if((numPiece == 0) and (Validator.__state[Validator.__x_new][Validator.__y_new] == "OO")):
				return True
		elif(Validator.__x_old == Validator.__x_new):
			if Validator.__y_new > Validator.__y_old:  # if new > old, travel from old to new
				temp = 1  # temp is direction to travel
			else:  # if new < old, travel from new to old
				temp = -1
			# Count num of piece between old position and new position
			for j in range(Validator.__y_old + temp, Validator.__y_new, temp):
				if(Validator.__state[Validator.__x_old][j] != "OO"):
					numPiece += 1
			# there is not piece between old and new position
			if((numPiece == 0) and (Validator.__state[Validator.__x_new][Validator.__y_new] == "OO")):
				return True
		# cannon capture a piece
		if(numPiece == 1 and Validator.__state[Validator.__x_new][Validator.__y_new] != "OO"):
			return True
		return False

	#   Check move of Soldier if invalid
	@staticmethod
	def checkSoldier():
		if(Validator.__y_old == Validator.__y_new):
			if(Validator.__turn == "R"):
				if(Validator.__x_old - Validator.__x_new == 1):
					return True
			elif(Validator.__turn == "B"):
				if(Validator.__x_new - Validator.__x_old == 1):
					return True
		elif(Validator.__x_old == Validator.__x_new):
			if((Validator.__y_new - Validator.__y_old == 1) or (Validator.__y_old - Validator.__y_new == 1)):
				if(Validator.__x_old < 5):
					if(Validator.__turn == "R"):
						return True
				else:
					if(Validator.__turn == "B"):
						return True
		return False
