WINDOWN_WIDTH = 960
WINDOWN_HEIGHT = 720
CONTAINER_WIDTH = 860	#container is layer that contain content such as board, setting,..
CONTAINER_HEIGHT = 620
BOARD_WIDTH = 600
BOARD_HEIGHT = 600
PIECE_SIZE = (60, 60)
ROW = 10	# Number row of board
COL = 9		# Number col or board
FPS = 60
PRIMARY = (2, 117, 216)
SUCCESS = (92, 184, 92)
INFOR = (91, 192, 222)
DANGER = (217, 83, 79)
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
# Symbol of piece
# prefix "B" is black piece, "R" is Red piece
# suffixes "S" is piece with border (chosed piece)
# middle symbol: A is Adviser, K is King, C is Cannon, N is Horse, B is Elephant, OO is NONE piece
# P is soldier, R is Chariot
PIECES = ['BA', 'BAS', 'BB', 'BBS', 'BC', 'BCS', 'BK', 'BKM',
		  'BKS', 'BN', 'BNS', 'BP', 'BPS', 'BR', 'BRS', 'OO',
		  'OOS', 'RA', 'RAS', 'RB', 'RBS', 'RC', 'RCS', 'RK',
		  'RKS', 'RKM', 'RN', 'RNS', 'RP', 'RPS', 'RR', 'RRS']

# initial state of chinese chess, matrix of pieces
# OO is cell that do not have piece
INITIALSTATE = [['BR', 'BN', 'BB', 'BA', 'BK', 'BA', 'BB', 'BN', 'BR'],
				['OO', 'OO', 'OO', 'OO', 'OO', 'OO', 'OO', 'OO', 'OO'],
				['OO', 'BC', 'OO', 'OO', 'OO', 'OO', 'OO', 'BC', 'OO'],
				['BP', 'OO', 'BP', 'OO', 'BP', 'OO', 'BP', 'OO', 'BP'],
				['OO', 'OO', 'OO', 'OO', 'OO', 'OO', 'OO', 'OO', 'OO'],
				['OO', 'OO', 'OO', 'OO', 'OO', 'OO', 'OO', 'OO', 'OO'],
				['RP', 'OO', 'RP', 'OO', 'RP', 'OO', 'RP', 'OO', 'RP'],
				['OO', 'RC', 'OO', 'OO', 'OO', 'OO', 'OO', 'RC', 'OO'],
				['OO', 'OO', 'OO', 'OO', 'OO', 'OO', 'OO', 'OO', 'OO'],
				['RR', 'RN', 'RB', 'RA', 'RK', 'RA', 'RB', 'RN', 'RR']]
SETTING_FILE = "manament\\setting.txt"