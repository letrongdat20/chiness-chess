import copy
from manament.validator import Validator
from manament.const import *

'''
	class that manage match by matrix of symbol
'''
class Match:
	
	def __init__(self):
		self.state = copy.deepcopy(INITIALSTATE)
		self.turn = 'R'
		self.winner = None
		self.history = [self.state]
		self.step = 0
		self.maxStep = 0

	'''
		if piece can move from src to des then return True
		else return False
		@param
			src {tuple or array} old position of pieces
			des {tuple or array} new position that want to move
	'''
	def move(self, src, des):
		x1, y1 = src
		x2, y2 = des
		if(self.state[x1][y1][0:1] == self.turn):
			# check if new position is invalid
			if(Validator.check(self.state, src, des)):
				self.state = copy.deepcopy(self.state)
				self.state[x2][y2] = self.state[x1][y1]
				self.state[x1][y1] = 'OO'
				self.changeTurn()
				# save history
				self.step += 1
				self.maxStep = self.step
				if self.step ==  len(self.history):
					self.history.append(self.state)
				else:
					self.history[self.step] = copy.deepcopy(self.state)
				return True
		return False

	# after move or reset game, game need to change turn of player
	def changeTurn(self):
		if(self.turn == 'B'):
			self.turn = 'R'
		else:
			self.turn = 'B'

	def getTurn(self):
		return copy.deepcopy(self.turn)

	def reset(self):
		self.state = copy.deepcopy(INITIALSTATE)
		self.turn = 'R'
		self.winner = None

	def getState(self):
		return copy.deepcopy(self.state)

	def isWin(self):
		if(self.winner is None):
			return False
		return True

	def getWinner(self):
		return self.winner

	def redo(self):
		if self.step < self.maxStep:
			self.step += 1
			self.state = self.history[self.step]
			self.changeTurn()

	def undo(self):
		if self.step > 0:
			self.step -= 1
			self.state = self.history[self.step]
			self.changeTurn()

	def nextGame(self):
		self.state = copy.deepcopy(INITIALSTATE)
		self.changeTurn()
		self.winner = None